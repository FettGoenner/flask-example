from flask import Flask, render_template, request, redirect
import datetime

import ast # to read dictionaries from files

app = Flask(__name__)

# Definiere die Hauptseite "/"
# Hier passiert nichts dynamisches
@app.route('/')
def index():
    return render_template('index.html')

# Definiere die Seite mit URL "/dynamic"
# Hier passiert dynamisches.
@app.route('/dynamic', methods=["GET", "POST"])
def dynamic():
    dt=str(datetime.date.today())

    # Wird der "Abschicken"-Button gedrückt?
    if request.method == "POST":
        # Hole den geschreibenen Text
        txt = str(request.form.get("txt"))
    else:
        # Sonst Filler:
        txt = str("Dein Text erscheint hier")

    # Die Felder {{date}} und {{text}} im HTML-Template werden hier beschrieben
    return render_template('dynamic.html', date=dt, text=txt)

# DICTIONARY-FUNKTIONALITÄT

def read():
    """
    Read a list of dictionaries from a file

    Returns: dicts (list) Liste von dictionaries
    """
    dicts = []
    # reading the data from the file
    with open(app.root_path + '/dicts.txt') as f:
        lines = f.readlines()
        d = ""
        for l in lines:
            if l[0] == '{':
                d = l
            elif l[0] == '}':
                d += l
                d = ast.literal_eval(d)
                dicts.append(d)
            else:
                d += l
    return dicts

def write():
    """
    Write a list of dictionaries to the file dicts.txt
    """
    with open(app.root_path + "/dicts.txt", 'w') as f:
        for d in dicts:
            f.write('{\n')
            for key, value in d.items():
                f.write("\"" + key + "\":\"" + str(value) + "\",\n")
            f.write('}\n')

dicts = read()

# Definiere Seite mit URL "/dict"
# Hier soll ein Dictionary angezeigt werden.
@app.route('/dict', methods=["GET", "POST"])
def dict():
    l = len(dicts)
    # Wird ein Button gedrückt?"
    if request.method == "POST":
        # "Leeres" dict hinzufügen, falls "Hinzufügen" gedrückt
        if request.form.get("add"):
            neu = {
                'vorname': '',
                'nachname': '',
                'kundennummer': '0',
                'alter': '0',
            }
            dicts.append(neu)
            return redirect(f"/edit/" + str(l))
        # Sonst editiere entsprechendes dict
        for i in range(len(dicts)):
            if request.form.get("edit" + str(i)):
                return redirect(f"/edit/{i}")
    # Leite weiter zum Editieren
    return render_template('dict.html', l=l, list=dicts)

# Definiere Seite mit URL "/edit/<nr>", i ist der Index des dicts
# Hier soll das Dictionary bearbeitet werden.
@app.route('/edit/<i>', methods=["GET", "POST"])
def edit(i):
    i = int(f"{i}") # hole Index 
    dict = dicts[i]
    # Wenn der "Speichern"-Button gedrückt wird,
    # beschreibe das Dictionary neu:
    if request.method == "POST":
        for key,val in dict.items():
            # Im HTML-Form heißen die Text-Inputs wie die Keys.
            dict[key] = request.form[key]
        write()
        # Dann zurück zur Übersicht
        return redirect('/dict')
    return render_template('edit.html', dict=dict, i=i)

if __name__ == "__main__":
    app.run(processes=1,debug=True)
