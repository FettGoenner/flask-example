# flask-example

Eine kleine Beispiel-Website zum Einstieg in Flask.
Dies ist die Homepage. Es gibt noch die "Dynamisches"-Page, um kleine dynamische Anwengungen vorzuführen.
Zuletzt gibt es noch die "Dictionary"-Page, die es ermöglicht Python-Dictionaries im Browser zu editieren.

Es ist noch unklar, wie sich das Projekt entwickelt. Ich lerne selbst noch Flask.

### Beispiel starten

Erst muss im Terminal die Flask-App lokalisiert werden, dann kann gestartet werden:

Bash:
```console
$ export FLASK_APP="/pfad/zu/flask-example/example.py"
$ flask run
```
ZSH:
```console
$ export FLASK_APP="/pfad/zu/flask-example/example.py"
$ python3 -m flask run
```
CMD: 
```console
> set FLASK_APP=/pfad/zu/flask-example/example.py
> flask run
```

Powershell: 
```console
> env:FLASK_APP = "/pfad/zu/flask-example/example.py"
> flask run
```

Nun die URL [`http://127.0.0.1:5000/`](http://127.0.0.1:5000/) im Browser aufrufen.

### Mehr Dokumentation

[Flask Quickstart](https://flask.palletsprojects.com/en/2.0.x/quickstart/)\
[HTML Templates 1](https://www.onlinetutorialspoint.com/flask/flask-simple-html-templates-example.html) \
[HTML Templates 2](https://pythonhow.com/html-templates-in-flask/)\
[noch mehr zu Templates](https://flask.palletsprojects.com/en/2.0.x/tutorial/templates/)
